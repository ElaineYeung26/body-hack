# Daily Journal


## Feb. 9, 2024
* We had our stand up and discussed what the goal is for today.
    - we plan to fix a few logic in our front end code such as getting the token and resetting state upon getting to the page if you logged out and log back in, old data won't be stored.
    - css on all of the components.
    - making a notes that deployment will be done the following week during stretch goal week.
    - clean up the entire code and check for console.logs, comments, unused imports and code.
    - running through the functionality of our web application
* Assisted with css and styling with Eddie and James.
* Reformatted entire repo and deleted comments and unused code.
* We had out stand down and talked a bit about stretch goals and deployment. 


## Feb. 8, 2024
* We had our stand up and discussed what the goal is for today.
    - all components are fully coded, we have all of our functionality done.
    - we talked about how to style the member home page.
        - the size of body heatmap.
        - limiting the appointment table to show future data and only 1 week ahead.
        - show a table that shows the category, the muscle groups for each category, and the dates.
    - we talked about styling all of our pages.
    - we talked about deployment.
    - we talked about the time schedule for today.
        - all styling done by evening token and then we deploy frontend and fill in the rest of the README.
* I went through the user stories and saw old data that is no long a part of our MVP, I went through the charts in the team's Notion file and updated the User Stories so Yaelin can re-populate it in the /docs.
    - I took out Stretch Goal related components.
    - Combined some components that are about the same things.
* I updated some components to better fit the README and our User Stories, naming conventions and etc.
* We all worked and gave suggestions on the css and how to format all the components.
* We had our stand down and I documented our progress and what we plan to do for the rest of the day as well as tomorrow before submission. I have slacked out the document.


## Feb. 7, 2024
* We had our stand up and discussed what the goal is for today.
    - James is helping take over one of Eddie's components and and is figuring out a way to edit circuits in the my workout list page. He still needs to figure out the body heat map and add that to the member's main page, editing the appointment list on the member and trainer main pages to show in a start time and end time format ( assume all appointments are 1 hour long), and add some css to the website.
    - Eddie is still working on the edit profile form.
    - Yaelin has to re-deploy the back end after some changes that James had made. She still has to update the README on anything deployment related and adding user stories and fixing some css on the main unauthenticated page.
    - I finished everything I was responsible for and majority of the team's work, I am going to help the team debug and pair program, be an extra eye.
* We had another stand up after lunch to get an update on progression and blockers. I updated the slack message from this morning to reflect the mid-day stand up discussion.
    - we talked about what should the trainer's home page upcoming appointment table should include.
        - we decided that there should not be a view client modal anymore as a lot of changes and functionality was made, the profile does not hold much information besides their name and email.
        - we decided to reflect that change and to just showcase the comments portion of the appointment form if applicable.
    - I updated the to-dos for everyone and talked out MVP with team members as well as debugging and the logic to code out certain parts of the components.
    -  we talked about the edit profile page for members, change password is not a modal and more of a conditional onClick to show 2 input tags and the handSubmit should have a conditional of if the change password was click then it would trigger two put request one for the generic first name, last name, and email changes and one for the password change. Otherwise, just trigger one put request for the generic first name, last name, and email.
    - we talked about the security measure of how the change password api endpoint was not secure and how we can refactor it.
    - we talked about how to implement the filters in the my workouts list page.
        - the session filter should just state personal or trainer, no need to list out all the trainers as most people stick to 1-2 trainers.
        - we discussed how to manage the state for the category filter, it should be a list and do a loop to check if each instance in the my workout list category section has any of the indexes in the filter than showcase it.
* I made changes to the wire-frame to reflect the changes to the trainers home page and I slacked out the updated wire-framing to everyone, updated it the /docs, relinked it correctly in the README, and updated it in the team's notion file.
* I updated my appointment form to only allow dates to be one day ahead, it does not make sense to allow members to schedule an appointment for the past or the current day as it would take time for trainers to approve the session.
* We had our stand down and I documented our progress and what we plan to do for the rest of the day as well as the rest of the week before submission. I have slacked out the document.


## Feb. 6, 2024
* We had our stand up and discussed what the goal is for today.
    - Eddie is still behind on his components, therefore we will spilt into pair programming and James will help him.
        - Since I figured out how to do the multi-select for categories I explained my code to Eddie so he can use that portion of my code in his component.
    - James is going to work on implementing the body heat-map for the different muscle groups and adding more css to make our front end look better.
    - Yaelin and I and completely finished with our components and is getting started on next step work such as README and CI/CD.
        - Yaelin is also adding the about dev portion on our main unauthenticated page.
            - adding name, title, linkedin link, and the repo gitlab link to this project.
        - Yaelin is also going to get started on CI/CD for our backend and frontend.
    - I removed navigating and redirecting users to the my workout list page after they successfully submit a workout form, after talking with the team, we feel that it makes more sense to just clear the form in case they want to fill out another rather than being redirected.
    - I merged my changes and the rest of the team git pulled.
* I am in pair programming with Yaelin and I assisted in documenting what should be inputted in the about dev portion of the unauthenticated page.
* I wrote out majority of the README. Below are the sections I fully wrote out.
    - ## Project BodyHack
    - ## Built With
    - ## Getting started
    - ## Install Extensions
    - ## Deliverables (Yaelin to add her Deployment part)
    - ## Project layout
    - ## Directories
* I created directories in the `docs` directory to better organize documentation and to not over flow the README.
    - API Design
        - All the details of each API endpoint
        - BodyHack ERD chart
    - User Stories
        - User Stories (To be filled in by Yaelin)
    - Wire-frame
        - Wire-frame spilt into the three rendered views
* I created a link to the README and the detail documentation in the `docs` sub directories.
* I assisted James in group muscles groups to our category groups for the body heat-map. I made a list of all the muscle groups in each category and slacked it to the team's slack channel.
* We had our stand down and I documented our progress and what we plan to do for the rest of the day as well as the rest of the week before submission. I have slacked out the document.
* I have documented what else needs to be done on the README regarding deployment and user stories which is Yaelin's responsibility and have slacked it to the team.
    - /docs/UserStories/UserStories.md
        - fill in with our user story documentation
    - README parts to fill in:
    *** If it is applicable include it but if not can delete, we can do through it together tomorrow if you want
        - ## Deliverables => [ ] Project is deployed to Caprover (BE, DB) & GitLab-pages (FE)
        - ## How to complete the initial deploy
        - ## Setup GitLab repo/project
        - ## Your GitLab pages URL
        - ## Initialize CapRover
        - ## Update GitLab CI/CD variables
        - ## Deploy it


## Feb. 5, 2024
* We had out stand up and talked about what we did over the weekend and the blockers we faced.
* I updated our to do lists for the day and the rest of the week and slacked it to everyone.
* We all took turns merging our code from the weekend and debugged as a group until we all have the same code on all of our local machines.
* We spilt off into pair programming to save time.
    - James and I in a pair and Eddie and Yaelin in a pair.
* James and I worked on adding the Error modal to my form components in the form did not submit.
    - I was able to add Error Modal to pop up if any errors happen during the post of the forms.
    - I was able to add a ErrorModal component and delete the old success modal after discussion with team.
    - I was able to add error modal css to the index.css file and delete the success modal css after discussion with team.
    - I updated the member home page to redirect to the appointment form if the create appointment button is clicked.
    - I updated the Nav to redirect to the workout form if the Add a workout button is clicked.
    - I was able to navigate the user to the member home page after the appointment form successfully submits.
    - I was able to navigate the user to the my workouts page after the workout form successfully submits.
    - I have tested out the appointment form and all its edge cases, it works!
    - I have tested all the functionality of the workout form and all the edge cases and it works!
    - Updated appointments api to hard code login required.
* Appointment and workout forms are complete and functional with error handling and edge cases!
* Eddie and Yaelin worked on Eddie's components and fix some bugs that Yaelin have.
* We had our stand down and I documented our progress and what we plan to do for the rest of the day as well as the rest of the week before submission. I have slacked out the document.


## Feb. 4, 2024
* I implemented some of my blockers after further testing.
    - I added the add and remove table functionalities to circuits.
    - I added inserting between table rows for circuits.
    - I fixed categories not saving and displaying properly.
    - I fixed trainer field being required if "trainer" is selected in the above option.
    - I created a function to submit the workout and its circuits.
* Blockers for appointment and workout forms:
    - user is not login error
    - placement of the modal


## Feb. 3, 2024
* I tried implementing some of my blockers with the advice and feedback givin by the team.
    - I added the global user id or in my case the member id in my create an appointment form using redux.
    - I added the modal css and the checkmark css to index.css.
    - I created the appointment modal component and imported into the appointment form.
    - I created the workout modal component and imported into the workout form.
    - I created a route in App.jsx to include my create appointment form component.
    - I created a route in App.jsx to include my create workout form component.
    - I have edited the wire-framing, removed all Nav bars from modals and resent it to the group's slack channel.
* Blockers for the appointment form:
    - user is not login error
    - placement of the modal
* Blockers for the workout form:
    - user is not login error
    - get submitting workouts and circuits functional
    - placement of the modal


## Jan. 31, 2024
* We had our stand up and discussed our plan for today.
    - Start pair programming
    - James will refactor our front end authentication and application to include Redux to manage global state of role.
* I drove with my partner, Eddie and was able to get most of the appointment form coded out.
    - I have a few blockers:
        - How to include frontend auth into the form in terms of the /mine part of the url post, get the member id in the form?
        - How to include the trainer id into the form to post?
        - How to include the success modal after submit?
    - Add redux to grab the member_id
    - Add my component to App.jsx
    - Edit the wire-frame to remove the Nav bar from the modal pop up.
* Eddie took the driver seat to start coding the my workouts view and we looked at documentation to create a multi-select input on React for the categories.
    - https://react-select.com/home#fixed-options
* I started coding the my workout form.
    - I have a few blockers:
        - How to include frontend auth into the form in terms of the /mine part of the url post, get the member id in the form?
        - How to input category data in the front end and make it a multiple select?
        - How do I do multiple simultaneous post request to create multiple circuits and link it to the current workout id before it is made? Then to submit the workout form as well?
            - to handle the the workout post request first then to handle all the circuit post requests.
        - How to include the success modal after submit?
    - Add redux to grab the member_id
    - Add my component to Nav.jsx
* We had our stand down and I documented our planned work for over the weekend, and next week to keep us on track to submitting our MVP on time. I have slacked out the document.


## Jan. 31, 2024
* We had our stand up and discussed our plan for today.
    - Add trainer data, we decided to just create a function / endpoint to do so in swagger due to security and hashed passwords.
    - Update our Notion API Design endpoints
    - Get started on front-end Auth, and finish setting up App.js and Nav.js, include prop-drilling.
    - Start pair programming of components
* I took the driver seat and created a function to create trainer data in swagger due to security and hashed passwords.
* I merged with main and the team git pulled and merged, so everything remote is all the same locally.
* Teammates testing it on their end and it all passed and everything is working.
* Yaelin updated our Notion API Design endpoints. and I double checked it.
* Eddie is formatting our package.json and renaming the files based on convention.
* James is taking the driver seat to conduct front-end authentication and finishing setting up the frontend, the Nav.jsx and App.jsx.
    - Everyone else helped contributed with research and debugging.
* We discussed that we might use Redux to conduct a global scope on the role attribute.


## Jan. 30, 2024
* We had our stand up and discussed our plan for today.
    - Eddie finished his unit test and it was reviewed by the group.
    - James finished adding a new functionality to change a member's password due to having it be hashed before replace that attribute in the member's database model.
    - We decided how to format / organize our components in the ghi folder.
        - Having a folder to render the different views / components (members, trainers, unauthenticated, nav)
        - main.jsx, App.jsx, nav.jsx
    - Eddie  and James will take the driver to set up our frontend and conduct frontend authentication.
    - Yaelin will take the driver seat after front-end authentication and set up is done to start coding the components she is responsible for.
        - Not login page(main page) / sign up and login forms
* We merged all backend work and made sure the pipeline passes with no conflicts.
* We started setting up our front end, creating and discussing how to implement App.js, Nav.js, importing bootstrap.
* We had our stand down and discussed what we needed to do tomorrow.
    - Finish front-end authentication
    - Finish setting up our front end.
    - Set up CI/CD for backend.
    - Pair programming our assigned components/pages.


## Jan. 29, 2024
* We had a stand up and discussed our progress and plan for the remainder of the project.
* James pulled down my branch and continued our work form Friday.
* The team decided to edit to our migrations.
    - we simplified our code for the trainers and members database table to just have id rather than trainers_id / members_id.
* James merged with main and I pulled the changes from main to my branch.
* I took the driver seat to then format and error check any changes.
* I tried out my trainers unit test and walked through errors with the team.
* I walked through testing every single endpoints' happy case and some edge cases on swagger with the team.
* I merged with my work from Elaine branch to main and the team git pulled and merged, so everything remote is all the same locally.
* Teammates testing it on their end and it all passed and everything is working.
* We had a walk through with Dalonte and he gave us feedback on our backend.
    - pull out hashed_password from members and trainers if not used.
    - we were able to pull it out for all the endpoints where hashed_password is not used.
    - we added new pydandic models for the update members endpoint that does require the hashed_passwords.
* We walked through Yaelin's unit test and made sure it was all passing. We merged after every person's unit test was successful.
* We discussed the work needed to be done for the front end and how to spilt that work up amongst members.
    - Elaine - Member view schedule a trainer and create a new workout forms
    - James - Member and Trainers home pages
    - Eddie - Members “My workout view” and member profile form
    - Yaelin - Un authenticated page/ sign up and login forms
* We had our stand down and discuss what we will be doing tomorrow. I have documented and updated the slack message of our plan, which I have pinned.


## Jan. 26, 2024
* I initiated the stand up meeting discussing what we completed yesterday and what the plan for today and the weekend and Monday should be.
* I was the driver today.
* I went through every single file and formatted everything, spacing, indents, names, json dictionaries, print statements are deleted, comments are deleted, and grouping pydantic models by the database model as well as naming them all the same.
* I made sure every endpoint that needed HTTP Exception handling had it and that every function also did error handling.
* I made sure we passed the pipeline.
* I initiated stand down and recapped what we did today and what was our blocker, what we needed to do over the weekend and what needs to be done on Monday.
* I updated James in Slack as he was sick today so he was up to date with the team's progress, plans, and decisions.


## Jan. 25, 2024
* We had out standup and discussed progress and blockers, we discussed a new issue with regards to one of the API endpoint for personal workouts.
    - The personal workout get_all method “/workouts/mine” passes all workouts, not just “mine”. We’ll need to change that so it checks what the logged in user’s member_id is and selects from the table based on that, or we filter it before we return it.
    - We figured out we needed to create another function under accounts to convert the log in token adn email to the member_id, so we can they filter the get all personal workouts by that member_id.
        - James will be taking the driver for that as he is in charge of Accounts.
* We continued working together as group programming for the appointments API endpoints.
* Eddie continued driving his portion of work for the backend, appointments.
* We all looked at Eddie's code and debugged together as we tested out each edge cases on Swagger.
* We then moved on to James as the driver and conducted pair programming for the issue noted during stand up.
* I assisted in all the debugging and walking through all the code and what our goal is, explaining pydantic models and how to structure our code, the flow of FastAPI.
* I documented all of our goals and progress, making sure we are still on schedule, like a project manager.
* Yaelin and I git pulled and merged, so everything remote is all the same locally.


## Jan. 24, 2024
* We had our stand up and discussed progression and if there was any blockers.
* We finished up the reconfiguring our endpoints with login required, JSON Dict format.
* We finished converting all of our tags and pydantics to be in a centralized place.
* James and Eddie both pushed their work and Yaelin and I navigated the merge conflicts.
* Eddie is the driver for today for coding out all the endpoints related to appointments and appointment hub.
* We were able to finish half of the endpoints and did a lot of debugging together.
* We had out stand down and determined what we will be doing tomorrow.
* Eddie merged his progress to main and the rest of us git pulled and merged, so everything remote is all the same locally.
* We all doubled check to see if everything is working on all of our machines.


## Jan. 23, 2024
* Yaelin was the driver today, she is taking charge on building out the entire CRUD for personal workouts and circuits.
* We discussed how the database tables are connected and how we will implement the create a personal workout and link circuits to it filtered by the member, if it is better to do it in the front-edn or back-end.
* We tested out each API endpoints for personal workouts and circuits in swagger.
* We discussed bugs together and fixed it together as a team, spelling, best practices, naming errors.
* We talked about how these calls will come to gether to achieve our goal, which is being able to create a personal workout log with circuits attached to it.
* Yaelin merged her branch with the main and everyone else git pulled and merged, so everything remote is all the same locally.
* Teammates testing it on their end and it all passed and everything is working.
* We discussed plans for the rest of today and the week, I noted it all down and slack it to the team.
* We reconfigured our code to be much cleaner and added the login required property to our API endpoints.
* We converted the pydantic models to be centralized in models.py.
* We converted the tags to be all centralized in main.py.
* We converted all of our API endpoints to be in the JSON Dict format.


## Jan. 22, 2024
* I was the driver for the meeting today.
* I was in charge of coding out the entire CRUD for the members and trainers API endpoints.
* I updated the main.py file to include the routers/members.py and routers/trainers.
* I created the queries/members.py and queries/trainers.
* I have coded out all the API Endpoints stated in our API Design for members, GET_ONE, GET_ALL, PUT, DELETE (POST is not needed as it is a part of SIGN UP).
* I have coded out all the API Endpoints stated in our API Design for TRAINERS, GET_ONE, GET_ALL ( POST, PUT, DELETE is not needed for the functionalities of our application).
* I have discussed with my team my implementation and talked through some edge cases and how to handle it.
* Ran tests with my team and discussed best practices with writing code for our team.
* Created a git Issue and discussed with team that we are not implementing them, instead we are going to detail what we did and for what feature or functionality when we create a merge request.
* I merged with my work from Elaine branch to main and the team git pulled and merged, so everything remote is all the same locally.
* Teammates testing it on their end and it all passed and everything is working.
* We discussed plans for the rest of today and the week.


## Jan. 19, 2024
* James is driving the rest of the building of the authorization files.
* We have all the Sign Up, Log In, Log Out done.
    - Yaelin, Eddie, and I are testing and fixing bugs, trying to catch some edge cases such as duplicate accounts, logging in with the wrong email or password.
* The team git pushed and pulled and merged, so everything remote is all the same locally. We ran test to ensure everything works in terms of the happy and edge cases.
* The team has discussed the plans and goals for the rest of today and next week. We have divided out the rest of the backend workload as in who is driving for which portion of the rest of the backend that we still need to code.
* I have updated the API Design on our team notion as I realized that we left out some end points after the changes we made. Yaelin double checked my work.
* James updated the ERD table as we realized we only had one many-to-many relationship rather than two.


## Jan. 18, 2024
* Eddie was the driver for building the authorization files.
* James, Yaelin, and I were navigators and we talked through conducting the authorization together.
* We started building out the Sign Up, Log In, Log Out functionalities for our site.


## Jan. 17, 2024
* James was the driver for building the migrations.
* Yaelin, Eddie, and I were navigators and we talked through our database and how to build the migration files together.
* The team git pushed and pulled and merged, so everything remote is all the same locally.


## Jan. 16, 2024
* I presented our project to the class and got good feedback, we did our stand up meeting and refactored our MVP.
* Yaelin forked and cloned the repo and completed all the start up.
* I cloned down the repo from gitlab and git pulled the updates made in the requirements.txt file from James.
* Updated our Group Agreement MVP and our group's Notion to reflect the changes.
    - removal of Workouts to be stretch goals.
* Updated our wire-framing to reflect the MVP and our group's changes.


## Jan. 12, 2024
* I took charged in creating the wire-framing and completed it with my group. Eddie took charge in creating a Notion for our group to put all the details of our project in. I have set up the template for our Notion (Wire-framing, API Design, Data Structure / ERD, Users Stories). James had populated and completed the API Design with the group. Yaelin and Eddie created the Users Stories page with the group.


## Jan. 9, 2024
* My group all settled on an idea for Project Gamma. We decided to do Project Body Hack, which is a workout tracker and appointment maker for a personal training app for gym members.
