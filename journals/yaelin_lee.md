# Daily Journal

## February 8, 2024

-   Added some stylings to the Profile page to look more presentable to the users and unified with the rest of the application colors and theme.
-   Updated the CI/CD section of the README

## February 7, 2024

-   Fixed the errors caused by the styling added to the App. Move the "meet the team" component from the footer to Homepage. Updated the styling of the container and components that goes into the "meet the team" section. Updated the LinkedIn logo to embed the member's linked in link, so the actual LinkedIn link address would not display below member role.
-   Added the user stories and frontend deployment information to the README.

## February 6, 2024

-   Updated the Unauthenticated Homepage
-   Added team members' information to the Footer
-   Set up for CI/CD
-   Deployed the backend development

In the footer, I added the 'MEET THE TEAM' section that contains team member's name, role, and LinkedIn link along with the LinkedIn log to the left. Footer.css is created to style the components in the footer. I also added the two additional components, Facility Amenities and Facility Photos, to the Homepage, and updated overall stylings and designs of the homepage components.

For CI/CD, I installed cirrus, set up the .gitlab-ci.yml file, deployed the database, created a production image in Gitlab's container registry, and deployed the api.

## February 5, 2024

-   Updated the SignupPage and fixed the error.

An error was found in the sign up page when a new user is trying to register with an email that's already existing in the system, the user is automatically logged in instead of receiving an error message about the duplicate email. A new account isn't getting created. I added another fetch call to get all of the existing member records in the handleSubmit. Then, it will check if the email entered in the sign up form exists in the fetched member records.

## February 1, 2024

-   I was the driver for the pair-programming session with James. Built the Sign Up form with react. The form successfully sends the form data to the back end but is failing to process the post method.

## January 31, 2024

-   As James driving, we worked on the login page and user authentication in frontend. The challenge is finding a way to pull user role when a token is generated.
-   I updated our team's API endpoint document as we made many changes for the endpoints and updated the response format.

## January 30, 2024

-   Set up for frontend development(Nav and App)
-   Attempted to implement Redux

With James driving, we developed the Nav bar components and linked them to App. I was the driver to setup the Redux and attempted to insert the trainer/member data through migrations. The team decided not to use the redux. Still couldn't figure out how to store the role from token.

## January 29, 2024

-   Created Unit Tests
-   Received the backend development feedback from Dalonte

James was the driver for developing the test_member_getall. Elaine was the driver for developing the test_member_update.
I was the driver for developing the test_personal_workouts. Through this experience, I learned to create mock data and call developed functions in test classes to test with the mocked data.

## January 26, 2024

-   Formatted the backend code

With Elaine driving, we formatted the backend code to pass the pipeline tests, formatted the get_all routers for each model to return data in dictionary format, and added exception handling where needed.

## January 25, 2024

With Eddie driving, we finished developing the appointments endpoints.

## January 24, 2024

-   Continued building the API Endpoints for the AppointmentsHub and Appointments models

With Eddie driving the development, the team collaborated on building the api endpoints and implementing the two appointment models. AppointmentHub model requires pulling the foreign key from another models, Member and Trainer.

## January 23, 2024

-   Built the API Endpoints for Personal Workouts and Circuits
-   Built the API Endpoints for Appointmentshub and Appointments

With the support of my team, I assumed the role of the primary developer to create the API endpoints for Personal Workouts and Circuits. For the Appointmentshub and Appointments model's API endpoint development, Eddie was the primary developer. We finished establishing the CRUD (Create, Read, Update, Delete) functionalities for all four models. Validated the functionalities using the fastAPI swagger by mocking the data insertion, modification, and deletion. We were also able to debug and resolve a number of errors that came up during the testing.

## January 22, 2024

-   Built the Members and Trainers API Endpoints
-   Discussed about the issue creation in GitLab

With Elaine being the driver of today's development session, we developed the API endpoints for the Members and Trainers by setting up the routers and queries for each database table, so that new data can be created and inserted to those two tables.

## January 19, 2024

-   Completed the development of the User authentication feature
-   Continued to discuss about the different ways to organize two user groups in our project

## January 18, 2024

Today, I worked on:

-   Authentication development with the team. Eddie was the driver, and the rest of the team was the navigator.

With the database tables created for members and trainers yesterday, we coded to add the authentication feature to our app,
so that both types of the users can login and navigate to the authenticated pages.

## January 17, 2024

Today, I worked on:

-   Converting three user stories, Member Login, Member Sign Up, and Trainer Login, to Issues and adding them to the Git repo.
-   Creating database tables and adding them to migrations while continuing to discuss about incorporating a many-to-many relationship between users and appointment.

Will be creating a mock-up tables in db-fiddle to see if our designed database tables (member and trainer entities hold a foreign key on
the account entity; appointment entity holds foreign keys on member and trainer) would work.

## January 16, 2024

Today, I worked on:

-   Setting up GitLab repo for Mod3 Gamma Project.

My team and I did setup our project repo in GitLab and practiced the Git workflow(push/pull, merge request).
