### Module3 Project Gamma

## Project BodyHack Team

-   Development Team, Roles and Responsibilities:

    -   Eddie Tsang: Project manager

        -   Back end: All API endpoints for Appointments
        -   Unit test: test_appointments.py
        -   Front end: My Personal Workouts List page, Edit Profile Form

    -   Elaine Yeung: Lead of Mental Breaks/ Diagrammer/ Project Manager

        -   Back end: All API endpoints for Members and Trainers
        -   Unit test: test_trainers.py
        -   Front end: Appointments Form, Personal Workouts Form, ErrorModal

    -   James Rushing: Team Lead

        -   Back end: All API endpoints for Authentication (integrating auth into all endpoints) and Migrations SQL
        -   Unit test: test_member_getall.py and test_member_update.py
        -   Front end: Frontend Auth, Member and Trainer login home pages

    -   Yaelin Lee: Business Analyst
        -   Back end: All API endpoints for Personal Workouts and Circuits
        -   Unit test: test_personal_workouts_create.py
        -   Front end: Sign up Form, Login Form, Unauthenticated Main page

## Project BodyHack

-   About Project BodyHack:

    -   BodyHack is a full-stack web application created for BodyHack gym. It allows gym members to visit the main page of the web application, sign up, and login for BodyHack's online fitness tracker and to set up appointments with BodyHack's personal trainers. It allows personal trainers to visit the main page of the web application and login to accept or decline appointments.

## Intended Market

-   This web application was created to target gyms for gym members and personal trainers of the gym to use.

-   BodyHack is designed to be user friendly. We have a simple home page for users to learn more about the gym, a button in the navbar to sign up or login. Upon login it redirects you automatically to your permission views based on your role, if you are a trainer or a gym member. If you are a trainer, you get redirected to the trainer home page where you can accept or decline incoming personal training appointment requests. A trainer can also see what their upcoming appointments and view information such as the date, time of the session, client's name, and client's comment if applicable. If you are a gym member, you get redirected to the member home page where you can see all your upcoming appointments that you have scheduled and their statuses, if it is accepted, pending, or rejected. The gym member can also click a button to schedule a personal training appointment. In the navbar, members can go to the home page, member home page, add a workout session and log what they did, view all of their workout histories and what they worked on, and edit their profile information.

## Built With

-   Postgres
-   FastAPI
-   Vite
-   React
-   Bootstrap
-   Redux
-   Docker

## Getting started

1. Fork this repository

2. Clone the forked repository onto your local computer:

    git clone https://gitlab.com/anonymous-chameleon/module3-project-gamma.git

3. In your local terminal, go to the Module3-Project-Gamma project directory, create and activate a virtual environment (.venv) in the project directory. Then install the requirements.

    - create a virtual environment:
      python -m venv ./.venv

    - activate your virtual environment:
      Windows with PowerShell: .\.venv\Scripts\Activate.ps1
      macOS: source ./.venv/bin/activate

    - install requirements:
      pip install -r requirements.txt

4. Build and run the project using Docker with these commands:

    docker volume create pg-admin
    docker volume create bodyhack-data
    docker-compose build
    docker-compose up

    -   After running these commands, make sure all of your Docker containers are running, you should have 4 containers running.

        -   `db-1`
        -   `pgadmin-1`
        -   `fastapi-1`
        -   `ghi-1`

5. Create a .env file in the Module3-Project-Gamma project directory and add these components:

    POSTGRES_PASSWORD=`{add your postgres password here}`
    POSTGRES_USER=`{add your postgres username here}`
    SIGNING_KEY=`{generate a signing key at this website: https://acte.ltd/utils/randomkeygen}`
    DATABASE_URL=`{add your postgres url here}`
    PGADMIN_EMAIL=`{add your pg-admin email here}`
    PGADMIN_DEFAULT_PASSWORD=`{add your pg-admin password here}`
    VITE_API_HOST=`{add your Vite api host url here}`
    VITE_API_URL=`{add your Vite api url here}`
    PUBLIC_URL=`{add your git deployment link here}`

-   View the project API endpoints in Swagger: http://localhost:8000/docs#/
-   View the project front end in Vite: http://localhost:5173/
-   View the project database tables in pg-admin: http://localhost:5050/browser/

## Install Extensions

-   Prettier: <https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode>
-   Black Formatter: <https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter>

## Deliverables

-   [ ] Wire-frame diagrams

    ![Img](/docs/Wireframe/Mod%203%20Workout%20Main%20Page%20Img.png)
    ![Img](/docs/Wireframe/Mod%203%20Workout%20Member%20Pages.png)
    ![Img](/docs/Wireframe/Mod%203%20Workout%20Trainer%20Page.png)

-   [ ] API documentation

    ![md](/docs/API%20Design/API_DesignEndpoints.md)
    ![img](/docs/API%20Design/BodyHack%20ERD.png)

-   [ ] Project is deployed to Cirrus (BE, DB) & GitLab-pages (FE)

    DB, BE, and FE deployment URLs are in the ### Deployment section.

-   [ ] User Stories / Project Management tracker

    ![Md](/docs/UserStories/UserStories.md)

-   [ ] Journals

    ![md](/journals/eddie_tsang.md)
    ![md](/journals/elaine_yeung.md)
    ![md](/journals/james_rushing.md)
    ![md](/journals/yaelin_lee.md)

## Project layout

-   We have two volumes:

    -   `bodyhack-data`
    -   `pg-admin`

-   We have four services:

    -   `fastapi`
    -   `ghi`
    -   `db`
    -   `pgadmin`

## Directories

-   The directories `docs` and `journals` are where the team put our documentation about our project and our project-journal entries.

-   The other directories, `ghi` and `api`, are services, where the team built the frontend and the backend.

-   Inside of `ghi` is a SPA (single page application). This app is written using the [Vite](https://vitejs.dev/) bundler and React. All of the components to render the SPA is inside the `src` directory in `ghi`. All of the images used to design the frontend is inside the `public` directory in `ghi`. The `components` directory holds all of the components for our SPA, it is separated by the different rendered views based off of authentication and role, as well as common files. There is the an `unauthenticated` directory for all the views an unauthenticated user can access. There is a `members` directory for all the views an authenticated user with the role of a member can access. Lastly, a `trainers` directory for the views an authenticated user with the role of a trainer can access. The `services`, `slices`, and `store` directories are all related to Redux and Redux Toolkit (RTK). Services holds the API slices for queries and mutations. Slices holds various slices to manage state storage and interface with the store, while the Store directory holds the store and the roleApi slice. We used AuthProvider from jwtdown-for-react to integrate authentication in the frontend as well as login credentials to receive a jwtdown-fastapi token from our authentication service in the backend.

-   Inside of `api` is a FastAPI application. Our entire backend api endpoints that connects the frontend to the backend to our databases are located in `main.py`. Each include_router in `main.py` is tied to `migrations` database table, a `queries` class function, a `routers` APIrouter function, pydantic models in `models.py`, and a unit test file that test at least an APIrouter function in the `tests` directory. We used jwtdown-fastapi to integrate authentication in the backend in `authenticator.py`. The Dockerfile and Dockerfile.dev run your migrations for you automatically.

### Deployment

************ Our Frontend Deployment will be performed during the stretch-goal week ************

## Setup GitLab repo/project

-   Setup Compute Timeout Limit

    Gitlab repo -> Settings -> CI/CD -> timeout -> enter 20 minutes

-   Remove fork relationship

    Settings -> General -> Advanced -> Remove fork relationship

## Run Tests

-   Run flake8 in the container

    Open Docker Desktop > fastapi container > Exec > Run "pip install flake8" > Run "flake8 ." and fix all issues

-   Run unit test

    Open Docker Desktop > fastapi container > Run "python -m pytest" > fix failed tests if needed

## Setup Dockerfile

-   Open the Dockerfile for deployment (not the one with .dev extension)

-   Make sure all of the necessary top-level files and the subdirectories in the api directory is copied as listed below:

    COPY requirements.txt requirements.txt
    COPY main.py main.py
    COPY models.py models.py
    COPY authenticator.py authenticator.py
    COPY queries queries
    COPY routers routers
    COPY migrations migrations

-   Make sure the following line is uncommented:

    CMD python -m migrations up && uvicorn main:app --host 0.0.0.0 --port 80

-   Make sure the following line is commented:

    CMD uvicorn main:app --host 0.0.0.0 --port 80

## Install and setup CLI

-   Navigate to " https://cirrus.mod3projects.com " and download the installer

-   Execute the Cirrus which will add "glv-cloud-cli" to your terminal's path

-   Restart the terminal as administrator

-   Run "glv-cloud-cli login" which will generate a token

    -   For troubleshooting or installing CLI in Mac OS, follow the instructions in this page:

    https://learn-2.galvanize.com/cohorts/3741/blocks/1908/content_files/build/01-ci-cd/05-cicd-install-cli.md

-   Add these GitLab CI/CD variables:

    -   PUBLIC_URL : this is your gitlab pages URL

    -   VITE_APP_API_HOST: enter "blank" for now

## Deploying a Postgres Database

-   Make sure you are logged in to the glv-cloud-cli

-   Run the following command to deploy the database(enter the user and password in {}):

    "glv-cloud-cli deploy -a db -i postgres:14.5-bullseye -e POSTGRES_DB=bodyhack -e POSTGRES_USER={SET UP NEW USER} -e POSTGRES_PASSWORD={SET UP NEW PASSWORD} -m /var/lib/postgresql -p 5432 -x=false"

-   Run "glv-cloud-cli list" to confirm that the database has been deployed

-   The returned data will give the following database url:

    postgresql://THE-USER:THE-PASSWORD@oct-2023-9-et-db-service.default.svc.cluster.local/bodyhack

-   To delete deployment, run "glv-cloud-cli deploy -a db -d"

## Create API Image

-   GitLab repo > Sidebar > Deploy > Container Registry

-   If there is no API Image in the registry:

    -   Open terminal > navigate to the project repository > Run "docker login registry.gitlab.com"

    -   Once authenticated, run " docker build -t registry.gitlab.com/anonymous-chameleon/module3-project-gamma ."

    -   Then, run "docker push registry.gitlab.com/anonymous-chameleon/module3-project-gamma"

-   Verify an image is created in the Container Registry in GitLab

## Deploying API

-   Before deploying the API, make sure you have a Database URL and a production image of the API in GitLab's container registry

-   In the Container Registry in GitLab, click the 'module3-project-gamma/api' folder

-   Click the 'copy' icon next to the 'latest' file

-   Paste the copied url to the end of the following command(make sure the copied url ends with 'latest'):

    glv-cloud-cli deploy -a api -i PASTE-FROM-CLIPBOARD

-   Build the rest of the command by adding the following to the end of the command above:

    -e SIGNING_KEY=ELJEU8Ejoijvra82\*3lrj -e CORS_HOST=https://anonymous-chameleon.gitlab.io -e DATABASE_URL=postgresql://deployment_admin:depAdmin@oct-2023-9-et-db-service.default.svc.cluster.local/bodyhack

-   Run the built command in the terminal where you're logged in as an administrator

-   Run "glv-cloud-cli list" command to view the record about the deployments

    -   There should be two deployments: database and API

-   To delete the deployment, run "glv-cloud-cli deploy -a api -d"

## Your GitLab pages URL

    https://anonymous-chameleon.gitlab.io/module3-project-gamma

## Frontend Deployment Build

    VITE_API_HOST: https://oct-2023-9-et-api.mod3projects.com.mod3projects.com

    PUBLIC_URL: https://anonymous-chameleon.gitlab.io/module3-project-gamma/

## Setup for Frontend Deployment

-   Lint the Frontend Code: Run "docker compose up" and fix the warnings mentioned in the ghi logs

-   Change the VITE_API_HOST and PUBLIC_URL variables in the env file to the actual URLs

-   Add a basename for React Router so the path will "/PROJECT_NAME"

-   Replace all <a> tags with React Router's <Link> component

-   Update the VITE_API_HOST variable in the docker-compose.yml to the actual production api URL for our project. Use the VITE_API_HOST where there are API Calls in our frontend code

-   Update the pages section of the .gitlab-ci.yml file, run frontend deployment
