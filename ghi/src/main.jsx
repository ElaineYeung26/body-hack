import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import './index.css';
import { BrowserRouter } from 'react-router-dom';
import { store } from './store/store';
import { Provider } from 'react-redux';

const domain = /https:\/\/[^/]+/
if (import.meta.env.VITE_PUBLIC_URL) {
    var basename = import.meta.env.VITE_PUBLIC_URL.replace(domain, '');
}
// const devbase = import.meta.env.VITE_DEV_URL

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <BrowserRouter basename={basename}>
            <Provider store={store}>
                <App />
            </Provider>
        </BrowserRouter>
    </React.StrictMode>
);
