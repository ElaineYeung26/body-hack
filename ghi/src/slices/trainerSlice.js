import { createSlice } from '@reduxjs/toolkit';

const trainersSlice = createSlice({
    name: 'trainers',
    initialState: {
        trainers: [],
        loading: 'idle',
        error: null,
    },
    reducers: {},
});

export default trainersSlice.reducer
