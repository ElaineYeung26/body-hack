import { createSlice } from '@reduxjs/toolkit';

const membersSlice = createSlice({
    name: 'members',
    initialState: {
        members: [],
        loading: 'idle',
        error: null,
    },
    reducers: {},
});

export default membersSlice.reducer;
