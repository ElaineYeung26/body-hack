import BackgroundCycle from './background';
import '../homepage.css';
import '../index.css';

export default function Home() {
    const teamMembers = [
        {
            name: 'Eddie Tsang',
            role: 'Project Manager',
            linkedIn: 'https://www.linkedin.com/in/eddie-tsang/',
        },
        {
            name: 'Elaine Yeung',
            role: 'Product Designer',
            linkedIn: 'https://www.linkedin.com/in/yatlamyeung/',
        },
        {
            name: 'James Rushing',
            role: 'Team Lead',
            linkedIn: 'https://www.linkedin.com/in/james-rushing/',
        },

        {
            name: 'Yaelin Lee',
            role: 'Business Analyst',
            linkedIn: 'https://linkedin.com/in/yaelinlee52',
        },
    ];

    return (
        <>
            <BackgroundCycle />
            <div className="px-4 py-4 text-center hero">
                <h1 className="section-header display-5 fw-bold mb-4">
                    BodyHack
                </h1>
                <div className="col-lg-8 mx-auto">
                    <p className="lead mb-4 fw-bold">
                        <span
                            style={{
                                backgroundColor: '#c0a25e',
                                padding: '5px',
                            }}
                        >
                            Unlock Your Ultimate Potential with BodyHack: Where
                            Fitness Meets Transformation!
                        </span>
                    </p>
                    <div className="hero-container p-4">
                        <p className="bodyhack-info-text">
                            Join our supportive community and embark on a
                            journey to redefine well-being. Our expert trainers
                            are dedicated to guiding you toward the best version
                            of yourself, regardless of your starting point. Be
                            part of a fitness family that values health,
                            vitality, and positive change.
                        </p>

                        <p
                            className="bodyhack-info-text"
                            style={{
                                textAlign: 'left',
                                fontSize: '25px',
                                marginTop: '20px',
                                color: '#290202',
                                textShadow: '1px 1px 1px #c0a25e',
                            }}
                        >
                            BodyHack is more than just a gym -
                        </p>

                        <p className="bodyhack-info-text">
                            it&apos;s a lifestyle of empowerment and
                            achievement. Let us help you sculpt the vibrant,
                            healthier &apos;you&apos; you&apos;ve always
                            envisioned.
                        </p>

                        <h3
                            className="bodyhack-info-text"
                            style={{
                                fontSize: '30px',
                                marginTop: '20px',
                                color: '#290202',
                                textShadow: '1px 1px 1px #c0a25e',
                            }}
                        >
                            Your transformation begins here!
                        </h3>
                    </div>
                </div>
                <div className="col-lg-8 mx-auto">
                    <div className="hero-container my-5">
                        <h2 className="section-header mt-2 fw-bold">
                            Facility Amenities
                        </h2>
                        <ul
                            className="list-unstyled fw-bold"
                            style={{
                                color: '#290202',
                                textShadow: '1px 1px 1px #c0a25e',
                            }}
                        >
                            <li>Gym Equipment</li>
                            <li>Group Fitness Classes</li>
                            <li>Personal Training</li>
                            <li>Locker Rooms</li>
                        </ul>
                    </div>
                </div>

                <div className="col-lg-8 mx-auto">
                    <div className="hero-container my-5">
                        <h2 className="section-header mt-2 fw-bold">
                            Facility Photos
                        </h2>
                        <div className="row">
                            <div className="col-md-4">
                                <img
                                    src="https://images.unsplash.com/photo-1540497077202-7c8a3999166f?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTR8fGd5bXxlbnwwfHwwfHx8MA%3D%3D"
                                    className="img-fluid rounded"
                                />
                            </div>
                            <div className="col-md-4">
                                <img
                                    src="https://images.unsplash.com/photo-1534405111774-ade5c6e8f02d?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTF8fGd5bSUyMGxvY2tlcnxlbnwwfHwwfHx8MA%3D%3D"
                                    alt="Facility Photo 2"
                                    className="img-fluid rounded"
                                />
                            </div>
                            <div className="col-md-4">
                                <img
                                    src="https://images.unsplash.com/photo-1623874514711-0f321325f318?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MzJ8fGd5bSUyMGxvY2tlcnxlbnwwfHwwfHx8MA%3D%3D"
                                    alt="Facility Photo 3"
                                    className="img-fluid rounded"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-10 mx-auto">
                    <div className="hero-container-team my-5">
                        <h2 className="section-header mt-2 fw-bold meet-the-team">
                            MEET THE TEAM
                        </h2>
                        <div className="row mt-3">
                            {teamMembers.map((member, index) => (
                                <div
                                    className="col-lg-3 col-md-4 col-sm-6"
                                    key={index}
                                >
                                    <div className="team-member">
                                        <h5 className="name profile-text">
                                            {member.name}
                                        </h5>
                                        <p className="role">{member.role}</p>
                                        {member.linkedIn && (
                                            <div className="linkedin-container">
                                                <div
                                                    className="linkedin-icon-container"
                                                    onClick={() =>
                                                        (window.location.href =
                                                            member.linkedIn)
                                                    }
                                                    role="button"
                                                    tabIndex={0}
                                                >
                                                    <img
                                                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/LinkedIn_Logo.svg/2560px-LinkedIn_Logo.svg.png"
                                                        alt="LinkedIn Icon"
                                                        className="linkedin-icon"
                                                    />
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
