import { useState } from 'react';
import {
    useGetCircuitsQuery,
    useEditCircuitMutation,
} from '../../services/circuits';

export default function CircuitsModal({ isOpen, onClose, workout_id, style }) {
    const API_URL = import.meta.env.VITE_API_HOST;
    const { data: circuits = [], isSuccess } = useGetCircuitsQuery(workout_id);
    const [editMode, setEditMode] = useState({});
    const [circuitForm, setCircuitForm] = useState({
        exercise: '',
        weight: '',
        reps_or_time: '',
        workout_id: workout_id,
        sets: 0,
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setCircuitForm((prevCircuitForm) => ({
            ...prevCircuitForm,
            [inputName]: value,
        }));
    };

    const getCircuit = async (circuitId) => {
        const url = `${API_URL}/workouts/mine/circuits/${circuitId}`;
        const fetchConfig = {
            method: 'GET',
            credentials: 'include',
            circuit_id: { circuitId },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const data = await response.json();
            setCircuitForm((prevCircuitForm) => ({
                ...prevCircuitForm,
                exercise: data.exercise,
                weight: data.weight,
                reps_or_time: data.reps_or_time,
                sets: data.sets,
            }));
        }
    };

    const [editCircuit] = useEditCircuitMutation();

    const handleEditClick = (circuitId, index) => {
        setEditMode((prevEditMode) => ({
            ...prevEditMode,
            [index]: !prevEditMode[index],
        }));
        getCircuit(circuitId);
    };

    const handleSubmit = async (circuitId, index) => {
        const data = { circuitId, circuitForm };
        try {
            await editCircuit(data);
            setEditMode((prevEditMode) => ({
                ...prevEditMode,
                [index]: false,
            }));
        } catch (e) {
            console.error(e);
        }
    };

    return (
        <div
            className={`circuit-modal ${isOpen ? 'open' : ''}`}
            style={{ ...style }}
            tabIndex="-1"
        >
            <div className="modal-content card-body-form">
                <div className="modal-header">
                    <button
                        className="close-button btn-custom"
                        style={{
                            marginLeft: 'auto',
                            paddingTop: 0,
                            paddingBottom: '2px',
                            fontSize: '1em',
                            justifyContent: 'center',
                            display: 'inline-block',
                            justifyContent: 'center',
                            fontWeight: 'bold',
                        }}
                        onClick={onClose}
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    {isSuccess && circuits.circuits.length > 0 ? (
                        <table className="table table-dark table-striped table-bordered">
                            <thead>
                                <tr
                                    className="text-center"
                                    style={{
                                        color: '#F4EBD0',
                                    }}
                                >
                                    <th>Exercise</th>
                                    <th>Weight</th>
                                    <th>Reps or Time</th>
                                    <th>Sets</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {circuits.circuits.map((circuit, index) => (
                                    <tr
                                        key={circuit.id}
                                        className="text-center"
                                    >
                                        {editMode[index] && circuitForm ? (
                                            <>
                                                <td className="text-center">
                                                    <input
                                                        onChange={
                                                            handleFormChange
                                                        }
                                                        type="text"
                                                        id="exercise"
                                                        name="exercise"
                                                        value={
                                                            circuitForm.exercise
                                                        }
                                                        className="form-control"
                                                    />
                                                </td>
                                                <td>
                                                    <input
                                                        onChange={
                                                            handleFormChange
                                                        }
                                                        type="text"
                                                        id="weight"
                                                        name="weight"
                                                        value={
                                                            circuitForm.weight
                                                        }
                                                        className="form-control"
                                                    />
                                                </td>
                                                <td>
                                                    <input
                                                        onChange={
                                                            handleFormChange
                                                        }
                                                        type="text"
                                                        name="reps_or_time"
                                                        id="reps_or_time"
                                                        value={
                                                            circuitForm.reps_or_time
                                                        }
                                                        className="form-control"
                                                    />
                                                </td>
                                                <td>
                                                    <input
                                                        onChange={
                                                            handleFormChange
                                                        }
                                                        type="text"
                                                        name="sets"
                                                        id="sets"
                                                        value={circuitForm.sets}
                                                        className="form-control"
                                                    />
                                                </td>
                                                <td>
                                                    <img
                                                        src="/greencheck.jpg"
                                                        alt="edit"
                                                        onClick={() =>
                                                            handleSubmit(
                                                                circuit.id,
                                                                index
                                                            )
                                                        }
                                                        style={{
                                                            height: '30px',
                                                            width: 'auto',
                                                            borderRadius: '50%',
                                                        }}
                                                    />
                                                </td>
                                            </>
                                        ) : (
                                            <>
                                                <td>{circuit.exercise}</td>
                                                <td>{circuit.weight}</td>
                                                <td>{circuit.reps_or_time}</td>
                                                <td>{circuit.sets}</td>
                                                <td>
                                                    <img
                                                        src="/editicon.png"
                                                        alt="edit"
                                                        onClick={() =>
                                                            handleEditClick(
                                                                circuit.id,
                                                                index
                                                            )
                                                        }
                                                        style={{
                                                            height: '30px',
                                                            width: 'auto',
                                                            maxWidth: '100%',
                                                        }}
                                                    />
                                                </td>
                                            </>
                                        )}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    ) : (
                        <p>No circuits loaded.</p>
                    )}
                </div>
            </div>
        </div>
    );
}
