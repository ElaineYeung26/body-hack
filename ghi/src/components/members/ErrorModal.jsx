export default function ErrorModal({ isOpen, message, onClose }) {
    return (
        <div
            className={`error-modal ${isOpen ? 'open' : ''}`}
            id="error-modal"
            tabIndex="-1"
            onClick={onClose}
        >
            <div className="modal-content">
                <div className="modal-header"></div>
                <div className="modal-body">
                    <p>{message}</p>
                </div>
                <div className="modal-footer"></div>
            </div>
        </div>
    );
}
