/* eslint-disable no-unused-vars */
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useGetOneMemberQuery } from '../../services/getmembers';
import ErrorModal from './ErrorModal';
import '../../index.css';
import { setFullName, selectId } from '../../slices/userSlice';

function Profile() {
    const API_URL = import.meta.env.VITE_API_HOST;
    const dispatch = useDispatch();
    const userId = useSelector(selectId);
    const { refetch } = useGetOneMemberQuery(userId);
    const [isChangePassword, setIsChangePassword] = useState(false);
    const [newPassword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');
    const [errorModal, setErrorModal] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const [profile, setProfile] = useState({
        id: userId,
        first_name: '',
        last_name: '',
        email: '',
    });

    const getProfileData = async (id) => {
        const myUserUrl = `${API_URL}/members/${id}`;
        const fetchConfig = { method: 'GET', credentials: 'include' };
        const myUserResponse = await fetch(myUserUrl, fetchConfig);
        if (myUserResponse.ok) {
            const userData = await myUserResponse.json();
            setProfile({
                id: userData.id,
                first_name: userData.first_name,
                last_name: userData.last_name,
                email: userData.email,
            });
        }
    };

    useEffect(() => {
        getProfileData(userId);
    }, []);

    const handleNewPassword = (e) => {
        setNewPassword(e.target.value);
    };

    const handleConfirmNewPassword = (e) => {
        setConfirmNewPassword(e.target.value);
    };

    const handleProfileChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setProfile({
            ...profile,
            [inputName]: value,
        });
    };

    function handleIsChangePasswordChange() {
        setIsChangePassword((isChangePassword) => !isChangePassword);
    }

    function handleCloseModal() {
        setErrorModal(false);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const memberUrl = `${API_URL}/members/${profile.id}`;
        const fetchConfig = {
            method: 'PUT',
            body: JSON.stringify(profile),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        };
        const response = await fetch(memberUrl, fetchConfig);
        if (response.ok) {
            const updatedProfile = await response.json();
            setProfile(updatedProfile);
            dispatch(
                setFullName({
                    first_name: updatedProfile['first_name'],
                    last_name: updatedProfile['last_name'],
                })
            );
            refetch();
        } else {
            setErrorModal(true);
            setErrorMessage('Error updating profile.');
        }

        if (isChangePassword) {
            if (newPassword === confirmNewPassword) {
                const changePasswordUrl = `${API_URL}/members/${profile.id}/change-password?password=${newPassword}`;
                const fetchConfig = {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    credentials: 'include',
                };
                const passwordChangeResponse = await fetch(
                    changePasswordUrl,
                    fetchConfig
                );
                if (passwordChangeResponse.ok) {
                    setIsChangePassword(false);
                    setNewPassword('');
                    setConfirmNewPassword('');
                }
            } else {
                setErrorModal(true);
                setErrorMessage('Password does not match.');
            }
        }
    };

    return (
        <div
            className="content-container-fluid member-background-container d-flex justify-content-center align-items-center"
            id="member-background"
        >
            <div className="container col-md-5 mb-5">
                <div className="card-body profile-card">
                    <h2 className="profile-text text-center fw-bold">
                        Edit My Profile
                    </h2>
                    <div className="form-group fw-bold profile-text">
                        <label>First Name</label>
                        <input
                            type="text"
                            value={profile.first_name}
                            name="first_name"
                            onChange={handleProfileChange}
                            className="form-control"
                        ></input>
                    </div>
                    <div className="form-group mt-3 fw-bold profile-text">
                        <label>Last Name</label>
                        <input
                            type="text"
                            value={profile.last_name}
                            name="last_name"
                            onChange={handleProfileChange}
                            className="form-control"
                        ></input>
                    </div>
                    <div className="form-group mt-3 fw-bold profile-text">
                        <label>Email Name</label>
                        <input
                            type="text"
                            value={profile.email}
                            name="email"
                            onChange={handleProfileChange}
                            className="form-control"
                        ></input>
                    </div>
                    {!isChangePassword && (
                        <div className="mb-3 mt-5 fw-bold profile-text">
                            <button
                                onClick={handleIsChangePasswordChange}
                                className="btn btn-custom btn-size-profile"
                            >
                                Change Password
                            </button>
                            <button
                                className="btn btn-custom float-end btn-size-profile"
                                onClick={handleSubmit}
                            >
                                Update
                            </button>
                        </div>
                    )}
                    {isChangePassword && (
                        <div className="mb-3 mt-3 fw-bold fw-bold profile-text">
                            <label>New Password</label>
                            <input
                                type="password"
                                value={newPassword}
                                name="newPassword"
                                onChange={handleNewPassword}
                                className="form-control"
                            ></input>
                            <label className="mt-3 fw-bold">
                                Confirm Password
                            </label>
                            <input
                                type="password"
                                value={confirmNewPassword}
                                name="confirmNewPassword"
                                onChange={handleConfirmNewPassword}
                                className="form-control"
                            ></input>
                            <div className="mb-3 mt-5">
                                <button
                                    onClick={handleIsChangePasswordChange}
                                    className="btn btn-custom btn-size-profile"
                                >
                                    Cancel
                                </button>
                                <button
                                    className="btn float-end btn-custom btn-size-profile"
                                    onClick={handleSubmit}
                                >
                                    Update
                                </button>
                            </div>
                        </div>
                    )}
                    {errorModal && (
                        <ErrorModal
                            isOpen={true}
                            message={errorMessage}
                            onClose={handleCloseModal}
                        />
                    )}
                </div>
            </div>
        </div>
    );
}

export default Profile;
