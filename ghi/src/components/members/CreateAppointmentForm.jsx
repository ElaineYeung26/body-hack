import { useState, useEffect } from 'react';
import { selectId } from '../../slices/userSlice';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useCreateAppointmentMutation } from '../../services/appointments';
import ErrorModal from './ErrorModal';

function CreateAppointmentForm() {
    const API_URL = import.meta.env.VITE_API_HOST;
    const navigate = useNavigate();
    const userId = useSelector(selectId);
    const [errorModal, setErrorModal] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [trainers, setTrainers] = useState([]);
    const [formData, setFormData] = useState({
        trainer_id: 0,
        member_id: userId,
        date: '',
        time: '',
        comments: '',
    });

    const getTrainers = async () => {
        const trainersUrl = `${API_URL}/trainers`;
        const response = await fetch(trainersUrl);
        if (response.ok) {
            const data = await response.json();
            setTrainers(data.trainers);
        }
    };

    useEffect(() => {
        getTrainers();
    }, []);

    const minDate = new Date(+new Date() + 86400000)
        .toISOString()
        .split('T')[0];

    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleCloseModal = () => {
        setErrorModal(false);
    };

    const [createAppointment, { isSuccess, isError }] =
        useCreateAppointmentMutation();

    const handleSubmit = async (event) => {
        event.preventDefault();
        await createAppointment(formData);
    };

    useEffect(() => {
        if (isSuccess) {
            navigate('/member');
        } else if (isError) {
            setErrorModal(true);
            setErrorMessage('Appointment fail to submit, please try again!');
        }
    }, [isSuccess, isError, navigate]);

    return (
        <div className="member-background-container">
            <div className="my-5 content-container container-fluid">
                <div className="container col-md-7">
                    <div className="card-body">
                        <h1
                            className="fw-bold profile-text"
                            style={{ textAlign: 'center' }}
                        >
                            Schedule a Personal Training Session
                        </h1>
                        <div className="row">
                            {errorModal && (
                                <ErrorModal
                                    isOpen={true}
                                    message={errorMessage}
                                    onClose={handleCloseModal}
                                />
                            )}
                            <div className="col">
                                <div>
                                    <div>
                                        <form
                                            onSubmit={handleSubmit}
                                            id="create-appointment-form"
                                            className=""
                                        >
                                            <div className="mb-3">
                                                <select
                                                    onChange={handleChange}
                                                    name="trainer_id"
                                                    id="trainer_id"
                                                    className="custom-input"
                                                    required
                                                >
                                                    <option value="">
                                                        Select a Trainer
                                                    </option>
                                                    {trainers.map((trainer) => {
                                                        return (
                                                            <option
                                                                key={trainer.id}
                                                                value={
                                                                    trainer.id
                                                                }
                                                            >
                                                                {
                                                                    trainer.first_name
                                                                }{' '}
                                                                {
                                                                    trainer.last_name
                                                                }
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input
                                                    value={formData.date}
                                                    onChange={handleChange}
                                                    placeholder=""
                                                    required
                                                    min={minDate}
                                                    type="date"
                                                    name="date"
                                                    id="date"
                                                    className="form-control"
                                                />
                                                <label
                                                    htmlFor="date"
                                                    className="text-dark"
                                                >
                                                    Date
                                                </label>
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input
                                                    value={formData.time}
                                                    onChange={handleChange}
                                                    placeholder=""
                                                    required
                                                    type="time"
                                                    name="time"
                                                    id="time"
                                                    className="form-control"
                                                />
                                                <label
                                                    htmlFor="time"
                                                    className="text-dark"
                                                >
                                                    Time
                                                </label>
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input
                                                    value={formData.comments}
                                                    onChange={handleChange}
                                                    placeholder=""
                                                    type="text"
                                                    name="comments"
                                                    id="comments"
                                                    className="form-control"
                                                />
                                                <label
                                                    htmlFor="comments"
                                                    className="text-dark"
                                                >
                                                    Comments
                                                </label>
                                            </div>

                                            <button className="btn btn-custom btn-custom-submit">
                                                Submit
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreateAppointmentForm;
