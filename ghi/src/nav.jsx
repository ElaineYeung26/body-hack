import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectRole } from './slices/userSlice';
import useLogout from './components/useLogout';

function Nav() {
    const userRole = useSelector(selectRole);

    const handleLogout = useLogout();

    return (
        <nav
            className="navbar navbar-expand-lg navbar-light light-bg"
            style={{ position: 'fixed', top: '0', width: '100%' }}
        >
            <div className="container-fluid d-flex justify-content-between align-items-center">
                {/* This link always shows. */}

                <div>
                    <Link to="/" className="logo-custom">
                        <img src="/bodyhacklogo3.jpg" alt="bodyhack logo" />
                    </Link>
                </div>

                {/* These links will show to an unauthenticated user. */}

                <div className="ms-auto">
                    {!userRole && (
                        <Link to="/signup">
                            <button className="btn btn-custom mx-2">
                                Sign Up
                            </button>
                        </Link>
                    )}

                    {!userRole && (
                        <Link to="/login">
                            <button className="btn btn-custom mx-2">
                                Login
                            </button>
                        </Link>
                    )}
                </div>

                {/* These links will show to a logged in member. */}

                {userRole === 'member' && (
                    <div className="me-auto">
                        <Link to="/member">
                            <button className="btn btn-custom mx-2">
                                Member Home
                            </button>
                        </Link>
                        <Link to="/workouts/create">
                            <button className="btn btn-custom mx-2">
                                Add a Workout
                            </button>
                        </Link>
                        <Link to="/workouts/mine">
                            <button className="btn btn-custom mx-2">
                                My Workouts
                            </button>
                        </Link>
                        <Link to="/profile">
                            <button className="btn btn-custom mx-2">
                                Edit Profile
                            </button>
                        </Link>
                    </div>
                )}
                {/* These links will show to a logged in trainer. */}

                {userRole === 'trainer' && (
                    <div className="me-auto">
                        <Link to="/trainer">
                            <button className="btn btn-custom mx-2">
                                Trainer Home
                            </button>
                        </Link>
                    </div>
                )}
                {(userRole === 'trainer' || userRole === 'member') && (
                    <button
                        className="btn btn-custom mx-2"
                        onClick={handleLogout}
                    >
                        Logout
                    </button>
                )}
            </div>
        </nav>
    );
}

export default Nav;
