import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

console.table(import.meta.env);
const API_URL = import.meta.env.VITE_API_HOST;

export const trainersApi = createApi({
    reducerPath: 'trainers',
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        credentials: 'include',
        headers: {
            'Content-type': 'application/json',
        },
    }),
    endpoints: (builder) => ({
        getTrainers: builder.query({
            query: () => "/trainers",
        }),
    }),
});

export const { useGetTrainersQuery } = trainersApi;
