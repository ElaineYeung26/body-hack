from models import WorkoutIn, WorkoutOut, AccountOutId, Error
from fastapi import HTTPException, status
from queries.pool import pool
from typing import Optional, Union, List
from pydantic import ValidationError


class WorkoutRepository:
    def get_one(self, workout_id: int) -> Optional[WorkoutOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                               category,
                               personal_or_trainer,
                               trainer,
                               date,
                               start_time,
                               end_time,
                               member_id
                        FROM personal_workouts
                        WHERE id = %s
                        """,
                        [workout_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_workout_out(record)
        except Exception:
            return {"message": "Failed to load your workout."}

    def delete(self, workout_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM personal_workouts
                        WHERE id = %s
                        """,
                        [workout_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, workout_id: int, workout: WorkoutIn
    ) -> Union[WorkoutOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE personal_workouts
                        SET category = %s,
                            personal_or_trainer = %s,
                            trainer = %s,
                            date = %s,
                            start_time = %s,
                            end_time = %s,
                            member_id = %s
                        WHERE id = %s;
                        """,
                        [
                            workout.category,
                            workout.personal_or_trainer,
                            workout.trainer,
                            workout.date,
                            workout.start_time,
                            workout.end_time,
                            workout.member_id,
                            workout_id,
                        ],
                    )
                    return self.workout_in_to_out(workout_id, workout)
        except Exception:
            return {"message": "Failed to update your workout."}

    def get_all_my_workouts(
        self, member_id: AccountOutId
    ) -> Union[Error, List[WorkoutOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM personal_workouts
                        WHERE member_id = %s
                        ORDER BY date;
                        """,
                        [member_id],
                    )
                    result = []
                    if db:
                        for record in db:
                            workout = self.record_to_workout_out(record)
                            result.append(workout)
                        return result
                    else:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="No workouts found.",
                        )
        except ValidationError as e:
            return {"Validation error: ", e}
        except Exception:
            return {"message": "Failed to load your workouts."}

    def create(self, workout: WorkoutIn) -> WorkoutOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO personal_workouts
                            (category,
                            personal_or_trainer,
                            trainer,
                            date,
                            start_time,
                            end_time,
                            member_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            workout.category,
                            workout.personal_or_trainer,
                            workout.trainer,
                            workout.date,
                            workout.start_time,
                            workout.end_time,
                            workout.member_id,
                        ],
                    )
                    id = result.fetchone()[0]
                return self.workout_in_to_out(id, workout)
        except Exception:
            return {"message": "Failed to create a new workout."}

    def workout_in_to_out(self, id: int, workout: WorkoutIn):
        old_data = workout.dict()
        return WorkoutOut(id=id, **old_data)

    def record_to_workout_out(self, record):
        try:
            return WorkoutOut(
                id=record[0],
                category=record[1],
                personal_or_trainer=record[2],
                trainer=record[3],
                date=record[4],
                start_time=record[5],
                end_time=record[6],
                member_id=record[7],
            )
        except ValidationError as e:
            return {"Validation error: ", e}
