from models import (
    MemberOut,
    MemberIn,
    Error,
)
from pydantic import ValidationError
from typing import List, Union, Optional
from queries.pool import pool


class MemberRepository:
    def get_one(self, member_id: int) -> Optional[Union[MemberOut, Error]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , first_name
                             , last_name
                             , email
                             , role
                        FROM members
                        WHERE id = %s;
                        """,
                        [member_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_member_out(record)
        except Exception as e:
            return ("Exception:", {e})

    def delete(self, member_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM members
                        WHERE id = %s;
                        """,
                        [member_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, member_id: int, member: MemberIn
    ) -> Union[MemberOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE members
                        SET first_name = %s
                          , last_name = %s
                          , email = %s
                        WHERE id = %s;
                        """,
                        [
                            member.first_name,
                            member.last_name,
                            member.email,
                            member_id,
                        ],
                    )
                    return self.member_in_to_out(member_id, member)
        except Exception as e:
            return ("Exception:", {e})

    def update_password(self, member_id: int, hashed_password: str) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        """
                        UPDATE members
                        SET hashed_password = %s
                        WHERE id = %s;
                        """,
                        [hashed_password, member_id],
                    )
                    return True
        except Exception as e:
            return ("Update password exception: --> ", {e})

    def get_all(self) -> Union[List[MemberOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , first_name
                             , last_name
                             , email
                             , role
                        FROM members
                        ORDER BY email;
                        """
                    )
                    return [
                        self.record_to_member_out(record) for record in result
                    ]
        except ValidationError:
            return Error(message="Validation error")
        except Exception:
            return Error(message="Exception")

    def member_in_to_out(self, member_id: int, member: MemberIn):
        old_data = member.dict()
        try:
            return MemberOut(id=member_id, **old_data)
        except ValidationError as e:
            return {"Validation error: ", e}
        except Exception as e:
            return {"member in to out exception: ", e}

    def record_to_member_out(self, record):
        try:
            return MemberOut(
                id=record[0],
                first_name=record[1],
                last_name=record[2],
                email=record[3],
                role=record[4],
            )
        except ValidationError as e:
            return {"Validation error: ", e}
        except Exception as e:
            return {"record to member out exception: ", e}
