steps = [
    [
        """
        CREATE TABLE IF NOT EXISTS circuits (
        id SERIAL PRIMARY KEY NOT NULL,
        exercise VARCHAR(100) NOT NULL,
        weight VARCHAR(100) NULL,
        reps_or_time VARCHAR(100) NOT NULL,
        workout_id INT REFERENCES personal_workouts(id) ON DELETE CASCADE,
        sets INT NOT NULL
        );
        """,
        """
        DROP TABLE circuits;
        """
    ]
]
