steps = [
    [
        """
        CREATE TABLE IF NOT EXISTS appointmentshub (
        member_id INT REFERENCES members(member_id) ON DELETE CASCADE,
        trainer_id INT REFERENCES trainers(trainer_id) ON DELETE CASCADE,
        appointment_id INT REFERENCES appointments(appointment_id) ON DELETE CASCADE,
        PRIMARY KEY (member_id, trainer_id, appointment_id)
        );
        """,
        """
        DROP TABLE appointmentshub;
        """
    ]
]
