from fastapi import APIRouter, Depends, Response, HTTPException, status
from typing import Optional, Union
from models import (
    AppointmentIn,
    AppointmentOut,
    AppointmentStatusIn,
    AppointmentList,
    Error,
)
from queries.appointments import AppointmentRepository
from authenticator import authenticator


router = APIRouter()


@router.get(
    "/trainers/mine/appointments",
    response_model=Union[AppointmentList, Optional[None]],
)
def get_trainer_all_appointments(
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        trainer_id = account_data["id"]
        result = repo.trainer_get_all(trainer_id)
        return {"appointments": result}
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to do that.",
        )


@router.get(
    "/members/mine/appointments",
    response_model=Union[AppointmentList, Optional[None]],
)
def get_member_all_appointments(
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        member_id = account_data["id"]
        print("Account data: ", member_id)
        result = repo.member_get_all(member_id)
        return {"appointments": result}
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to do that.",
        )


@router.post(
    "/members/mine/appointments", response_model=Union[AppointmentOut, Error]
)
def create_appointment(
    appointment: AppointmentIn,
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        return repo.create(appointment)
    else:
        print("Create appt account data:", account_data)
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create that record.",
        )


@router.get(
    "/members/mine/appointments/{appointment_id}",
    response_model=Union[AppointmentOut, None],
)
def get_appointment(
    appointment_id: int,
    response: Response,
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        appt = repo.get_appointment(appointment_id)
        if appt is None:
            response.status_code = 404
        return appt
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to do that.",
        )


@router.put(
    "/members/mine/appointments/{appointment_id}",
    response_model=Union[AppointmentOut, None],
)
def update_appointment(
    appointment_id: int,
    appointment: AppointmentIn,
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        return repo.update_appointment(
            appointment_id=appointment_id, appointment=appointment
        )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to edit that record.",
        )


@router.put(
    "/trainer/mine/appointments/{appointment_id}",
    response_model=Union[AppointmentOut, None],
)
def trainer_update_appointment(
    appointment_id: int,
    appointment: AppointmentStatusIn,
    response: Response,
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        appt = repo.update_appointment_status(
            appointment_id=appointment_id, appointment=appointment
        )
        if appt is None:
            response.status_code = 404
        return appt
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to edit that record.",
        )


@router.delete(
    "/members/mine/appointments/{appointment_id}", response_model=bool
)
def delete_appointment(
    appointment_id: int,
    repo: AppointmentRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        return repo.delete(appointment_id)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to delete that record.",
        )
