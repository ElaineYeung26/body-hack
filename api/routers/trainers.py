from fastapi import APIRouter, Depends, Response
from typing import Union, Optional
from models import (
    TrainerOut,
    TrainerList,
    Error
)
from queries.trainers import (
    TrainerRepository,
)


router = APIRouter()


@router.get(
    "/trainers",
    response_model=Union[TrainerList, Error],
)
def get_all(
    repo: TrainerRepository = Depends(),
):
    return {"trainers": repo.get_all()}


@router.get(
    "/trainers/{trainer_id}",
    response_model=Union[Optional[TrainerOut], Error],
)
def get_one_trainer(
    trainer_id: int,
    response: Response,
    repo: TrainerRepository = Depends(),
) -> TrainerOut:
    trainer = repo.get_one(trainer_id)
    if trainer is None:
        response.status_code = 404
    return trainer
