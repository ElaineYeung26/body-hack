from fastapi import APIRouter, Depends, Response, HTTPException, status
from typing import Union, Optional
from authenticator import authenticator
from models import WorkoutIn, WorkoutOut, WorkoutList, Error
from queries.personal_workouts import WorkoutRepository


router = APIRouter()


@router.post(
    "/workouts/mine",
    response_model=Union[WorkoutOut, Error],
)
def create_personal_workouts(
    workout: WorkoutIn,
    repo: WorkoutRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data and account_data["id"] == workout.member_id:
        return repo.create(workout)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create that record.",
        )


@router.get(
    "/workouts/mine",
    response_model=Union[Error, WorkoutList],
)
def get_all_my_workouts(
    repo: WorkoutRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        member_id = account_data["id"]
        result = repo.get_all_my_workouts(member_id)
        if result:
            return {"personal_workouts": result}
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="No records founds.",
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in retrieve those records.",
        )


@router.put(
    "/workouts/mine/{workout_id}",
    response_model=Union[WorkoutOut, Error],
)
def update_personal_workout(
    workout_id: int,
    workout: WorkoutIn,
    repo: WorkoutRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> Union[Error, WorkoutOut]:
    if account_data:
        return repo.update(workout_id, workout)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create that record.",
        )


@router.delete(
    "/workouts/mine/{workout_id}",
    response_model=bool,
)
def delete_personal_workout(
    workout_id: int,
    repo: WorkoutRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> bool:
    if account_data:
        return repo.delete(workout_id)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create that record.",
        )


@router.get(
    "/workouts/mine/{workout_id}",
    response_model=Union[WorkoutOut, Optional[None]],
)
def get_one_personal_workout(
    workout_id: int,
    response: Response,
    repo: WorkoutRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> WorkoutOut:
    if account_data:
        workout = repo.get_one(workout_id)
        if workout is None:
            response.status_code = 404
        return workout
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to retrieve that record.",
        )
