from fastapi import APIRouter, Depends, Response, HTTPException, status
from typing import Union, Optional
from authenticator import authenticator
from models import CircuitIn, CircuitOut, CircuitList, Error
from queries.circuits import CircuitRepository


router = APIRouter()


@router.post(
    "/workouts/mine/{workout_id}/circuits",
    response_model=Union[CircuitOut, Error],
)
def create_circuits(
    circuit: CircuitIn,
    repo: CircuitRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        return repo.create(circuit)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create that record.",
        )


@router.get(
    "/workouts/mine/circuits/{circuit_id}",
    response_model=Union[CircuitOut, Optional[None]],
)
def get_one_circuit(
    circuit_id: int,
    response: Response,
    repo: CircuitRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> CircuitOut:
    if account_data:
        circuit = repo.get_one(circuit_id)
        if circuit is None:
            response.status_code = 404
        return circuit
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create that record.",
        )


@router.delete(
    "/workouts/mine/circuits/{circuit_id}",
    response_model=bool,
)
def delete_circuit(
    circuit_id: int,
    repo: CircuitRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> bool:
    if account_data:
        return repo.delete(circuit_id)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in.",
        )


@router.put(
    "/workouts/mine/circuits/{circuit_id}",
    response_model=Union[CircuitOut, Error],
)
def update_circuit(
    circuit_id: int,
    circuit: CircuitIn,
    repo: CircuitRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> Union[Error, CircuitOut]:
    if account_data:
        return repo.update(circuit_id, circuit)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in.",
        )


@router.get(
    "/workouts/mine/{workout_id}/circuits",
    response_model=Union[Error, CircuitList],
)
def get_all_circuits(
    workout_id: int,
    repo: CircuitRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        return {"circuits": repo.get_all_circuits(workout_id)}
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in.",
        )
