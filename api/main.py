from fastapi import FastAPI
from authenticator import authenticator
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import (
    accounts,
    members,
    trainers,
    personal_workouts,
    circuits,
    appointments,
)


app = FastAPI()
app.include_router(authenticator.router, tags=["Account"])
app.include_router(accounts.router, tags=["Account"])
app.include_router(members.router, tags=["Members"])
app.include_router(trainers.router, tags=["Trainers"])
app.include_router(personal_workouts.router, tags=["Personal Workout"])
app.include_router(circuits.router, tags=["Circuits"])
app.include_router(appointments.router, tags=["Appointments"])


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST"),
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
