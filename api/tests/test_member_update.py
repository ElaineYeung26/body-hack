from fastapi.testclient import TestClient
from main import app
from models import MemberIn, MemberOut
from queries.members import MemberRepository
from authenticator import authenticator


client = TestClient(app)

member = {
    "first_name": "James",
    "last_name": "Bond",
    "email": "james@bodyhack.com",
}


class MockMemberRepository:
    def update(self, member_id: int, member: MemberIn):
        return MemberOut(
            id=6,
            first_name="James",
            last_name="Bond",
            email="james@bodyhack.com",
        )


def mock_get_current_account_data():
    return {"id": 6, "email": "james@bodyhack.com"}


def test_update_member(member_id=6, member=member):
    # test /members/{member_id} put endpoint

    # Arrange
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[MemberRepository] = MockMemberRepository
    # Act
    response = client.put(f"/members/{member_id}", json=member)

    # Assert
    assert response.status_code == 200

    # Cleanup
    app.dependency_overrides = {}
